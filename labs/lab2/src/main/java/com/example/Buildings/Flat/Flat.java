package com.example.Buildings.Flat;

public class Flat {
    private double square;
    private int rooms;
    public Flat(){
        this.square = 50;
        this.rooms = 2;
    }

    public Flat(double square){
        this.rooms = 2;
        this.square = square;
    }

    public Flat(double square, int rooms){
        this.square = square;
        this.rooms = rooms;
    }

    public int getRooms(){
        return this.rooms;
    }

    public void setRooms(int rooms){
        this.rooms = rooms;
    }

    public double getSquare(){
        return this.square;
    }

    public void setSquare(double square){
        this.square = square;
    }
}