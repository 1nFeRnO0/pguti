package com.example.Buildings.Dwelling;

import com.example.Buildings.DwellingFloor.DwellingFloor;
import com.example.Buildings.Flat.*;


public class Dwelling {
    private DwellingFloor floors[];
    public Dwelling(int num, int count[]){
        this.floors = new DwellingFloor[num];
        for(int i = 0; i < num; i++){
            Flat flats[] = new Flat[count[i]] ;
            for(int j = 0; j < count[i]; j++) {
                flats[j] = new Flat();
            }
            this.floors[i] = new DwellingFloor(flats);
        }
    }

    public Dwelling(DwellingFloor floors[]){
        this.floors = floors;
    }

    public int getFloorsCount(){
        return floors.length;
    }

    public int getFlatsCount(){
        int sum = 0;
        for (DwellingFloor dwellingFloor : floors) {
            sum += dwellingFloor.getFlatsCount();
        }
        return sum;
    }

    public double getFlatsSquare(){
        double sum = 0;
        for (DwellingFloor dwellingFloor : floors) {
            sum += dwellingFloor.getFlatsSquare();
        }
        return sum;
    }

    public int getRoomsCount(){
        int sum = 0;
        for (DwellingFloor dwellingFloor : floors) {
            sum += dwellingFloor.getFlatsRooms();
        }
        return sum;
    }

    public DwellingFloor[] getDwellingFloors(){
        return this.floors;
    }

    public DwellingFloor getDwellingFloor(int num){
        if(num < floors.length && num >= 0)
            return floors[num];
        System.out.println("There is no such floor");
        System.exit(0);
        return null;
    }

    public void setDwellingFloor(int num, DwellingFloor floor){
        if(num < floors.length && num >= 0)
            floors[num] = floor;
        System.out.println("There is no such floor");
        System.exit(0);
    }

    public Flat getFlat(int num){
        if(num < this.getFlatsCount() && num >= 0){
            int flats = 0;
            int fl = 0;
            for (DwellingFloor dwellingFloor : floors) {
                if(num >= flats){
                    flats += dwellingFloor.getFlatsCount();
                    fl++;
                }
                else break;
            }
            return floors[fl - 1].getFlat(num - (flats - floors[fl - 1].getFlatsCount()));
            }
        System.out.println("There is no such flat");
        System.exit(0);
        return null;
    }

    public void addFlat(int num, Flat flat){
        if(num >= this.getFlatsCount()){
            floors[floors.length - 1].addFlat(flat, num - (this.getFlatsCount() - floors[floors.length - 1].getFlatsCount()));
        }
        else{
            System.out.println("This flat already exist");
            System.exit(0);
        }
    }

    public void deleteFlat(int num){
        if(num < this.getFlatsCount() && num >= 0){
            floors[floors.length - 1].deleteFlat(num - (this.getFlatsCount() - floors[floors.length - 1].getFlatsCount()));
        }
        else{
            System.out.println("There is no such flat");
            System.exit(0);
        }
    }

    public Flat getBestSpace(){
        Flat f = new Flat();
        for (DwellingFloor dwellingFloor : floors) {
            Flat fl = dwellingFloor.getBestSpace();
            f = f.getSquare() > fl.getSquare() ? f : fl;
        }
        return f;
    }

    public Flat[] getSortedFlats(){
        int count = this.getFlatsCount();
        Flat f[] = new Flat[count];
        for(int i = 0; i < count; i++){
            f[i] = this.getFlat(i);
        }
        for(int i = 1; i < f.length; i++){
            if(f[i].getSquare() > f[i - 1].getSquare()){
                Flat temp = f[i];
                f[i] = f[i - 1];
                f[i - 1] = temp;
                for(int z = i - 1; (z - 1) >= 0; z--){
                    if(f[z].getSquare() > f[z - 1].getSquare()){
                        temp = f[z];
                        f[z] = f[z - 1];
                        f[z - 1] = temp;
                    } else{
                        break;
                    }
                }
            }
        }
        return f;
    }
}
