package com.example;

public class MySecondClass{
    private int a,b;

    public MySecondClass(){
        a = 1;
        b = 2;
    }

    public void setA(int a){this.a = a;}
    public void setB(int b){this.b = b;}

    public int getA(){return this.a;}
    public int getB(){return this.b;}

    public int summer(){return a+b;}
}

