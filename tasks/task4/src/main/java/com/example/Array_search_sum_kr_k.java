package com.example;

import java.util.Arrays;

class Searching{
    void printPairSum (int[] array, int sum) {
        int first = 0;
        int last = array.length - 1;
        boolean flag = false;

        while (first < last) {
          int s = array[first] + array[last];
          if (s == sum) {
            int[] ansArray = new int[2];
            ansArray[0] = array[first];
            ansArray[1] = array[last];
            System.out.println(Arrays.toString(ansArray));
            flag = true;
            break;
          }else{
            if (s < sum) first++;
            else last--;
          }
        }
        
        if (flag == false){
            int[] ansArray = new int[2];
            System.out.println(Arrays.toString(ansArray));
            }
      }
}